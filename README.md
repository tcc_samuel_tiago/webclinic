# Tcc_Samuelh-Tiago
# Horts

#
# Nome da Equipe
Tiago Rosa Paiano e Samuel Henrique Wenceslau 

#
# Turma
353

#
# Tema
Site Hospítalar

#
# Introdução
Sera um software de cadastro e agendamento do usuario para ter consultas médicas.
# Objetivo Geral
    * Desenvolver um software que contém informações do usuario e que agende uma consulta de modo rápido e prático.

#
# Objetivo Específico
    * Facilitar que o usuario tenha rapidez no seu agendamento de consultas
    * Permitir o cadastro de pacientes no site 
    * Auxiliar o paciente com informações sobre os sintomas de algumas doenças.
