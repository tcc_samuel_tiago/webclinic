<!---------------Mostra o botao "SAIR" se o usuario estiver logado--------------->
<?php
@session_start();

$logado = false;

if (isset($_SESSION['id_usuario'])) {
  $logado = true;
}
?>
<!-------------------------------------------------------------------------------->

<!doctype html>
<html lang="pt-br">

<!------------------LOGO NA BARRA DE GUIA------------------>
<link rel="shortcut icon" href="img/logo.png" />
<!--------------------------------------------------------->

<!---------------------CSS sobre---------------------->

<head>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <meta charset="utf-8">
  <title>Web Clinic</title>
  <link rel="stylesheet" href="sobre.css">
</head>
<!----------------------------------------------------->
<div class="img">

  <head>
    <!-----------Para aceitar acentuacoes-------------->
    <meta charset="UTF-8" />
    <!------------------------------------------------>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!------------------------------------------------------------>

    <!----------------------Fonte do NAV---------------------------------------------------->
    <link rel="stylesheet" href="estilo2.css">
    <link href="https://fonts.googleapis.com/css?family=Exo&display=swap" rel="stylesheet">
    <!-------------------------------------------------------------------------------------->


     <!---------------------------------------NAV----------------------------------------------------------------->

  <!------------------------------COR do NAV------------------------->

  <nav class="navbar navbar-expand-lg navbar navbar-primary bg-body">
    <!--------------------------------------------------------------->

    <div class="container">

      <!----------------------------------LOGO NO NAV--------------------------------------->
      
      <a class="navbar-brand" href="indexLocal.php"><img src="img/logo.png" width="30" height="30"></a>
      <!------------------------------------------------------------------------------------>

      <!-----------------Nome "WEB CLINIC" no NAV------------->
      <a class="navbar-brand" href="indexLocal.php"><B>Web Clinic</B></a>
      <!------------------------------------------------------>

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu_resp">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="menu_resp">
        <ul class="navbar-nav nav ml-auto">

          <li class="nav-item"><a href="indexLocal.php" class="nav-link">Home</a></li>

          </li>
          <li class="nav-item">
            <?php
            if ($logado) {
              echo '<a class="nav-link" href="deslogar.php" tabindex="-1" aria-disabled="true">Sair</a>';
            } else {
              echo '<a class="nav-link" href="login.php" tabindex="-1" aria-disabled="true">Login</a>';
            }
            ?>
          </li>


          <li class="nav-item"><a href="sobre.php" class="nav-link">Sobre</a></li>

          <li class="nav-item dropdown">
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Solicitar a consulta</a>
            <div class="dropdown-menu bg-muted" id="submenu">
              <a href="calendario.php" class="dropdown-item">Agende</a>
              <a href="admin_agendamento.php" class="dropdown-item">Status Agendamento</a>
              <a href="admin.php" class="dropdown-item">Adm</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="admin.php">criar uma pagin aqui</a>
            </div>
          </li>

          <li class="nav-item"><a href="contato.php" class="nav-link">Fale conosco</a></li>

          <li class="nav-item"><a href="socios.php" class="nav-link">Sócios</a></li> 
          <li class="nav-item"><a href="direitos_respon.php" class="nav-link">Objetivos e Deveres</a></li>
          <li class="nav-item"><a href="prevencoes.php" class="nav-link">Prevenções</a></li>

          <a href=https://www.facebook.com> <img src="img/facebook.png"><i class="fa fa-facebook"></i></a>
          <a href=https://www.instagram.com> <img src="img/instagram.png"><i class="fa fa-instagram"></i></a>
          <a href=https://www.instagram.com> <img src="img/2828.png" style="margin-left:12px; margin-top:3px;"><i class="fa fa-instagram"></i></a>

        <!-------  <li class="nav-item dropdown">
            <a href="admin.php" class="nav-link dropdown-toggle" data-toggle="dropdown">Paginas de adm</a>
            <div class="dropdown-menu bg-muted" id="submenu">
              <a href="admin_agendamento.php" class="dropdown-item">Status Agendamento</a>
              <a href="admin.php" class="dropdown-item">Adm</a>
          </li>

          <a href=https://www.facebook.com> <img src="img/facebook.png"><i class="fa fa-facebook"></i></a>
          <a href=https://www.instagram.com> <img src="img/instagram.png"><i class="fa fa-instagram"></i></a>
        </ul>
        ------------->
      </div>
    </div>
  </nav>
  <!---------------------------------------------------------FIM NAV--------------------------------------------->

    <!----------------------------------------------------INICIO DO SOBRE-------------------------------------------------------------------------->
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

    <!------------------------------------------------------------FIM DO SOBRE-------------------------------------------------------------------->

    <!-- Meta tags Obrigatórias -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">


    <!-- JavaScript (Opcional) -->
    <!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    </body>

</html>