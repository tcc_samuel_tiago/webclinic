<!---------------Mostra o botao "SAIR" se o usuario estiver logado--------------->
<?php
@session_start();

$logado = false;

if (isset($_SESSION['id_usuario'])) {
  $logado = true;
}
?>
<!-------------------------------------------------------------------------------->


<!doctype html>
<html lang="pt-br">
<meta charset="utf-8">
<!---------------ICONE DO SITE----------------->
<link rel="shortcut icon" href="img/logo.png" />
<!--------------------------------------------->

<link rel="stylesheet" href="calendario.css">
<!--------------CHAMANDO O PARALAX------------->
<link rel="stylesheet" href="estilo2.css">
<link href="https://fonts.googleapis.com/css?family=Exo&display=swap" rel="stylesheet">
<!--------------------------------------------->


 <!---------------------------------------NAV----------------------------------------------------------------->

  <!------------------------------COR do NAV------------------------->

  <nav class="navbar navbar-expand-lg navbar navbar-primary bg-body">
    <!--------------------------------------------------------------->

    <div class="container">

      <!----------------------------------LOGO NO NAV--------------------------------------->
      
      <a class="navbar-brand" href="indexLocal.php"><img src="img/logo.png" width="30" height="30"></a>
      <!------------------------------------------------------------------------------------>

      <!-----------------Nome "WEB CLINIC" no NAV------------->
      <a class="navbar-brand" href="indexLocal.php"><B>Web Clinic</B></a>
      <!------------------------------------------------------>

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu_resp">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="menu_resp">
        <ul class="navbar-nav nav ml-auto">

          <li class="nav-item"><a href="indexLocal.php" class="nav-link">Home</a></li>

          </li>
          <li class="nav-item">
            <?php
            if ($logado) {
              echo '<a class="nav-link" href="deslogar.php" tabindex="-1" aria-disabled="true">Sair</a>';
            } else {
              echo '<a class="nav-link" href="login.php" tabindex="-1" aria-disabled="true">Login</a>';
            }
            ?>
          </li>


          <li class="nav-item"><a href="sobre.php" class="nav-link">Sobre</a></li>

          <li class="nav-item dropdown">
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Solicitar a consulta</a>
            <div class="dropdown-menu bg-muted" id="submenu">
              <a href="calendario.php" class="dropdown-item">Agende</a>
              <a href="admin_agendamento.php" class="dropdown-item">Status Agendamento</a>
              <a href="admin.php" class="dropdown-item">Adm</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="admin.php">criar uma pagin aqui</a>
            </div>
          </li>

          <li class="nav-item"><a href="contato.php" class="nav-link">Fale conosco</a></li>

          <li class="nav-item"><a href="socios.php" class="nav-link">Sócios</a></li> 
          <li class="nav-item"><a href="direitos_respon.php" class="nav-link">Objetivos e Deveres</a></li>
          <li class="nav-item"><a href="prevencoes.php" class="nav-link">Prevenções</a></li>

          <a href=https://www.facebook.com> <img src="img/facebook.png"><i class="fa fa-facebook"></i></a>
          <a href=https://www.instagram.com> <img src="img/instagram.png"><i class="fa fa-instagram"></i></a>
          <a href=https://www.instagram.com> <img src="img/2828.png" style="margin-left:12px; margin-top:3px;"><i class="fa fa-instagram"></i></a>

        <!-------  <li class="nav-item dropdown">
            <a href="admin.php" class="nav-link dropdown-toggle" data-toggle="dropdown">Paginas de adm</a>
            <div class="dropdown-menu bg-muted" id="submenu">
              <a href="admin_agendamento.php" class="dropdown-item">Status Agendamento</a>
              <a href="admin.php" class="dropdown-item">Adm</a>
          </li>

          <a href=https://www.facebook.com> <img src="img/facebook.png"><i class="fa fa-facebook"></i></a>
          <a href=https://www.instagram.com> <img src="img/instagram.png"><i class="fa fa-instagram"></i></a>
        </ul>
        ------------->
      </div>
    </div>
  </nav>
  <!---------------------------------------------------------FIM NAV--------------------------------------------->

<!doctype html>
<html lang="pt=br">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>Web Clinic</title>

  <!-- Bootstrap -->
  <!<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <!<script src="https://ajaxgoogleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js">
      </script>
      <!<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js">
        </script>

        <!<link href="css/bootstrap-datepicker.css" rel="stylesheet">
          <!<script src="js/bootstrap-datepicker.min.js">
            </script>
            <!<script src="js/bootstrap-datepicker.pt-BR.min.js">
              </script>


</head>

<body>
  <!----------------- SITUÇAO DA DOENÇA --------------------->
  <div class="container" method="post">
    <form action="agendamento.php" method="post">

      <link href="https://fonts.googleapis.com/css?family=Exo&display=swap" rel="stylesheet">
      <h1>Situação da Doença</h1>
      <h5>selecione a doença que voce tem clique para ter mais informaçoes sobre ela</h5>
      <br>

      <div class="container">
        <div class="row">
          <div class="col-sm">
            <h2><a href="https://www.portaleducacao.com.br/conteudo/artigos/enfermagem/doencas-cerebrovasculares/28198%22%3E">1. Doenças cerebrovasculares</a> <input type="radio" name="doenca" value="1"><br> </h2>
            <div class="progress">
              <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
            </div>
            <br>

            <h2><a href="https://www.hcor.com.br/hcor-explica/cardiologia/infarto-do-miocardio-adote-habitos-que-protegem-o-seu-coracao-para-poder-evita-lo/%22%3E">2. Infarto agudo do miocárdio</a> <input type="radio" name="doenca" value="3"><br> </h2>
            <div class="progress">
              <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 90%"></div>
            </div>
            <br>

            <h2><a href="https://drauziovarella.uol.com.br/doencas-e-sintomas/pneumonia/%22%3E">3.Pneumonia</a> <input type="radio" name="doenca" value="4">
              <div class="progress">
                <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 78%"></div>
              </div>
              <br>
            </h2>

            <h2><a href="https://www.endocrino.org.br/o-que-e-diabetes/%22%3E">4. Diabetes mellitus</a> <input type="radio" name="doenca" value="5"><br> </h2>
            <div class="progress">
              <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 78%"></div>
            </div>
            <br>

            <h2><a href="https://renastonline.ensp.fiocruz.br/cid/doencas-hipertensivas-i10-i15%22%3E">5. Doenças hipertensivas </a> <input type="radio" name="doenca" value="5"><br> </h2>
            <div class="progress">
              <div class="progress-bar progress-bar-striped progress-bar-animated bg-orange" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 70%"></div>
            </div>
          </div>

          <div class="col-sm">
            <h2><a href="">6. Bronquite, enfisema, asma</a> <input type="radio" name="doenca" value="7"><br> </h2>
            <div class="progress">
              <div class="progress-bar progress-bar-striped progress-bar-animated bg-DeepPink    " role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 60%"></div>
            </div>
            <br>
            <h2><a href="">7. Insuficiência cardíaca</a> <input type="radio" name="doenca" value="8"><br> </h2>
            <div class="progress">
              <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 55%"></div>
            </div>
            <br>
            <h2><a href="">8. Câncer de pulmão</a> <input type="radio" name="doenca" value="9"><br> </h2>
            <div class="progress">
              <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 45%"></div>
            </div>
            <br>
            <h2><a href="">9. Cirrose e doenças crônicas fígado</a> <input type="radio" name="doenca" value="10"><br> </h2>
            <div class="progress">
              <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 38%"></div>
            </div>
            <br>
            <h2><a href="">10. Câncer de estômago</a> <input type="radio" name="doenca" value="11"><br> </h2>
            <div class="progress">
              <div class="progress-bar progress-bar-striped progress-bar-animated bg-DeepSkyBlue" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 34%"></div>
            </div>
          </div>
        </div>
      </div>

      <br><br><br>


      <div class="container">
        <h3>Defina o medico </h3>
        <form>
          <div class="form-group">
            <label for="medico">Selecione o médico que você deseja:</label>
            <select class="form-control" id="medico">
              <option>Dr. Drauzio Varella (especializado em Doenças cerebrovasculares) </option>
              <option>Dr. Samuel Henrique (especializado em Infarto agudo do miocárdio)</option>
              <option>Dr. Tiago Paiano (especializado em Pneumonia)</option>
              <option>Dr. Ramon Venson (especializado em Diabetes mellitus)</option>
              <option>Dr. Isaque Pereira (especializado em Doenças hipertensivas)</option>
              <option>Dra. Maria Eduarda Mendes (especializado em Bronquite, enfisema, asma)</option>
              <option>Dr. Daiana Fernandes (especializado em Insuficiência cardíaca) </option>
              <option>Dra. Marina Silva (especializado em Câncer de pulmão)</option>
              <option>Dra. Jessica Ramalho (especializado em Câncer de estômago)</option>
            </select>
          </div>
        </form>
      </div>
      <br>

      <div class="container">
        <h3>Defina uma hora e data para voce ser atendido</h3>
        <h1 class="Cadastrar Data"></h1>

        <form class="form-horizontal">
          <div class="form-group" method="post">
            <label class="col-sm-2 control-label">Data e Hora</label>
            <div class="col-sm-10">

              <div class="input-group date">
                <input type="date" class="form-control" name="data">
                <input type="time" class="form-control" name="hora">
                <div class="input-group-addon">
                  <span class="glyphicon glyphicon-th"></span>
                </div>
              </div>

            </div>
          </div>
          <br>
          <h3>O que você sente ?</h3>
          <div class="row">
            <div class="col-sm-12"><textarea id="mensagem" class="form-control" name="mensagem" required="" placeholder="O que voce sente ? fale para nos para indicarmos voce para o melhor profissional"></textarea></div>
          </div>
          <br><br>
          <div class="form-group" method="post">
            <div class="col-sm-12"><button class="btn btn-success" type="submit">Cadastro</button></div>
          </div>
      </div>
  </div>
  </form>

  </div>

  <script type="text/javascript">
    $('#exemplo').datepicker({
      format: 'dd/mm/yyyy',
      language: "pt-BR",
    });
  </script>
</body>

</html>

<!--------------------BOTAO DE FINALIZAR AGENDAMENTO--------------------------->

<!-------------------https://www.youtube.com/watch?v=M7AsS4oTk78--------------->

<!-- Meta tags Obrigatórias -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<title>Web Clinic</title>
</head>

<body>

  <!-- JavaScript (Opcional) -->
  <!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
</body>

</html>