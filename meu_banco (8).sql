-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 06-Nov-2019 às 15:47
-- Versão do servidor: 5.6.15-log
-- PHP Version: 5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `meu_banco`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `agendamento`
--

CREATE TABLE IF NOT EXISTS `agendamento` (
  `id_agendamento` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `id_doenca` int(11) NOT NULL,
  `id_medico` int(11) NOT NULL,
  `data` date NOT NULL,
  `hora` time NOT NULL,
  `status` varchar(128) NOT NULL,
  `mensagem` varchar(220) NOT NULL,
  PRIMARY KEY (`id_agendamento`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Extraindo dados da tabela `agendamento`
--

INSERT INTO `agendamento` (`id_agendamento`, `id_usuario`, `id_doenca`, `id_medico`, `data`, `hora`, `status`, `mensagem`) VALUES
(1, 0, 0, 0, '0000-00-00', '00:00:00', 'Confirmado', 'af'),
(2, 0, 0, 0, '0000-00-00', '00:00:00', 'Confirmado', 'af'),
(3, 0, 0, 0, '0000-00-00', '00:00:00', 'Confirmado', 'dores em todo meu corpinhos gostosinhos'),
(4, 0, 0, 0, '0000-00-00', '00:00:00', 'Confirmado', 'dores em todo meu corpinhos gostosinhos'),
(5, 0, 0, 0, '0000-00-00', '00:00:00', 'Confirmado', 'dores em todo meu corpinhos '),
(6, 0, 0, 0, '0000-00-00', '00:00:00', 'Confirmado', 'pontada.'),
(7, 0, 0, 0, '0000-00-00', '00:00:00', '', 'dafe'),
(8, 0, 0, 0, '0000-00-00', '00:00:00', '', 'dfdfg'),
(9, 0, 0, 0, '0000-00-00', '00:00:00', '', 'rassdas'),
(10, 0, 0, 0, '2019-10-27', '00:00:00', '', '00'),
(11, 0, 0, 0, '2019-10-27', '12:00:00', '', '00asdasd'),
(12, 0, 0, 0, '2019-10-23', '12:12:00', '', 'dores'),
(13, 13, 1, 0, '2019-10-01', '21:21:00', '', 'asasas'),
(14, 13, 1, 0, '2019-10-05', '15:15:00', '', 'dor'),
(15, 13, 3, 0, '2019-10-11', '23:51:00', '', 'koko'),
(16, 13, 1, 0, '2019-10-26', '21:02:00', '', 'sasa'),
(17, 13, 1, 0, '2019-10-12', '21:21:00', '', 'asasa'),
(18, 13, 10, 0, '2019-10-12', '16:56:00', '', '2661'),
(19, 13, 1, 0, '2019-10-26', '08:59:00', 'Pendente', 'kokok'),
(20, 13, 11, 0, '2019-10-17', '21:02:00', '', '212'),
(21, 13, 0, 0, '0000-00-00', '00:00:00', '', ''),
(22, 13, 0, 0, '0000-00-00', '00:00:00', '', ''),
(23, 21, 4, 0, '2019-11-07', '10:51:00', '', 'Dor');

-- --------------------------------------------------------

--
-- Estrutura da tabela `doencas`
--

CREATE TABLE IF NOT EXISTS `doencas` (
  `id_doenca` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `foto` blob NOT NULL,
  `texto` varchar(2555) NOT NULL,
  PRIMARY KEY (`id_doenca`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `fale_conosco`
--

CREATE TABLE IF NOT EXISTS `fale_conosco` (
  `id` int(220) NOT NULL AUTO_INCREMENT,
  `nome` varchar(220) NOT NULL,
  `email` varchar(220) NOT NULL,
  `telefone` varchar(14) NOT NULL,
  `mensagem` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `fale_conosco`
--

INSERT INTO `fale_conosco` (`id`, `nome`, `email`, `telefone`, `mensagem`) VALUES
(1, 'tiago', 'tiago@gmail.com', '48996511649', 'nananana'),
(2, 'tiago', 'tiago@gmail.com', '34425437', 'arruma ai'),
(3, 'carlinhos', 'carlinhos@gmail.com', '984008901', 'fsdfad'),
(4, 'carlinhos', 'carlinhos@gmail.com', '984008901', 'Arruma o login'),
(5, 'fsd', 'csc@fds.df', 'sdcds', 'scds');

-- --------------------------------------------------------

--
-- Estrutura da tabela `medico`
--

CREATE TABLE IF NOT EXISTS `medico` (
  `id_medico` int(11) NOT NULL AUTO_INCREMENT,
  `id_doenca` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  PRIMARY KEY (`id_medico`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Extraindo dados da tabela `medico`
--

INSERT INTO `medico` (`id_medico`, `id_doenca`, `nome`) VALUES
(1, 0, 'Dr. Drauzio Varella'),
(2, 0, 'Dr. Samuel Henrique'),
(3, 0, 'Dr. Tiago Paiano'),
(4, 0, 'Dr. Ramon Venson'),
(5, 0, 'Dr. Isaque Pereira'),
(6, 0, 'Dra. Maria Eduarda Mendes'),
(7, 0, 'Dr. Daiana Fernandes'),
(8, 0, 'Dra. Marina Silva'),
(9, 0, 'Dra. Jessica Ramalho');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(200) NOT NULL,
  `senha` varchar(32) NOT NULL,
  `email` varchar(200) NOT NULL,
  `privilegio` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `usuario`, `senha`, `email`, `privilegio`) VALUES
(14, 'robson', '123', 'robshow@hotmail.com', 0),
(15, 'carlos', 'car12', 'carlos@gmail.com', 0),
(13, 'tiago', 'paiano001', 'tiago@gmail.com', 1),
(16, 'carlinhos', 'caca123', 'carlinhos@gmail.com', 0),
(17, 'aa', '123', 'aa@gmail.com', 0),
(18, 'Robert', 'robertnr23', 'robertnr32@gmail.com', 0),
(19, 'Robert', 'robertnr23', 'robertnr32@gmail.com', 0),
(20, 'Robert', 'robert23', 'robertnr32@gmail.com', 0),
(21, 'Alan Rigoni', '26022002', 'alan_rigoni@hotmail.com', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
