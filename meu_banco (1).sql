-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 25-Out-2019 às 22:09
-- Versão do servidor: 5.6.15-log
-- PHP Version: 5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `meu_banco`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `agendamento`
--

CREATE TABLE IF NOT EXISTS `agendamento` (
  `id_agendamento` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `id_doenca` int(11) NOT NULL,
  `id_medico` int(11) NOT NULL,
  `data` date NOT NULL,
  `hora` time NOT NULL,
  `status` varchar(128) NOT NULL,
  `mensagem` varchar(220) NOT NULL,
  PRIMARY KEY (`id_agendamento`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Extraindo dados da tabela `agendamento`
--

INSERT INTO `agendamento` (`id_agendamento`, `id_usuario`, `id_doenca`, `id_medico`, `data`, `hora`, `status`, `mensagem`) VALUES
(1, 0, 0, 0, '2019-10-09', '12:12:00', '', 'sdasd'),
(2, 0, 0, 0, '2019-10-10', '21:02:00', '', 'fafa'),
(3, 0, 0, 0, '2222-02-12', '12:12:00', '', 'cvvdfg'),
(4, 0, 0, 0, '0000-00-00', '00:00:00', '', 'sad'),
(5, 0, 0, 0, '0000-00-00', '00:00:00', '', 'asd'),
(6, 0, 0, 0, '0000-00-00', '00:00:00', '', 'asda'),
(7, 0, 1, 0, '0000-00-00', '00:00:00', '', 'scc'),
(8, 0, 1, 0, '0000-00-00', '00:00:00', '', 'asd'),
(9, 13, 1, 0, '0000-00-00', '00:00:00', '', 'sa'),
(10, 13, 0, 0, '2020-04-09', '18:38:00', '', 'dores na cabeÃ§a de cima'),
(11, 13, 0, 0, '2019-10-18', '18:38:00', '', 'dores na cabeÃ§a de ccimaaaaa'),
(12, 13, 0, 0, '0000-00-00', '00:00:00', '', 'n'),
(13, 13, 10, 0, '0000-00-00', '00:00:00', '', 'aa');

-- --------------------------------------------------------

--
-- Estrutura da tabela `doencas`
--

CREATE TABLE IF NOT EXISTS `doencas` (
  `id_doenca` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `foto` char(220) NOT NULL,
  `texto` varchar(2555) NOT NULL,
  PRIMARY KEY (`id_doenca`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Extraindo dados da tabela `doencas`
--

INSERT INTO `doencas` (`id_doenca`, `nome`, `foto`, `texto`) VALUES
(1, 'Doenças cerebrovasculares\r\n\r\n', '', 'As doenças cerebrovasculares são um grupo de disfunções cerebrais relacionadas com a doença dos vasos sanguíneos que fornecem sangue ao cérebro. A hipertensão arterial é a causa mais importante que pode causar danos ao revestimento dos vasos sanguíneos ( o endotélio) expondo o colágeno onde as plaquetas se juntam para dar início a um processo de reparação que nem sempre é completo e perfeito. Com a hipertensão permanente, as alterações à estrutura dos vasos sanguíneos deixando-os mais estreitos, rígidos, deformados e desiguais, sendo mais vulneráveis às flutuações da pressão arterial. Uma queda na pressão arterial durante o sono pode levar à redução acentuada do fluxo sanguíneo no estreitamento dos vasos sanguíneos, causando um acidente vascular cerebral de manhã, com um aumento súbito da pressão arterial, podendo para além disso provocar uma hemorragia intracraniana. Principalmente as pessoas que são idosas, diabéticas, fumantes ou que tenham doenças do coração têm um risco à doença cerebrovascular. Todas as doenças relacionadas com a disfunção arterial podem ser classificadas como doença macrovascular. Este é um estudo simplista pelo qual as artérias estão bloqueadas pelos depósitos gordos ou por um coágulo. Os resultados da doença cerebrovascular pode incluir um AVC, ou mesmo por vezes um acidente vascular cerebral hemorrágico. Isquemia ou outras disfunções dos vasos sanguíneos podem afectar durante um acidente vascular cerebral.'),
(3, 'Infarto agudo do miocárdio', '', 'Infarto agudo do miocárdio é necrose miocárdica resultante de obstrução aguda de uma artéria coronária. Os sintomas incluem desconforto torácico com ou sem dispneia, náuseas e diaforese. O diagnóstico é efetuado por ECG e pela existência ou ausência de marcadores sorológicos. O tratamento consiste em drogas antiplaquetárias, anticoagulantes, nitratos, betabloqueadores, estatinas e terapia de reperfusão. Para IM com elevação do segmento ST, reperfusão de emergência com drogas fibrinolíticas, intervenção percutânea ou, ocasionalmente, cirurgia de revascularização miocárdica. Para IM sem elevação do segmento ST, a reperfusão é por meio de intervenção percutânea ou cirurgia de revascularização do miocárdio.'),
(4, 'Pneumonia', '', 'Pneumonia é uma infecção que se instala nos pulmões, órgãos duplos localizados um de cada lado da caixa torácica. Pode acometer a região dos alvéolos pulmonares onde desembocam as ramificações terminais dos brônquios e, às vezes, os interstícios (espaço entre um alvéolo e outro).\r\nBasicamente, pneumonias são provocadas pela penetração de um agente infeccioso ou irritante (bactérias, vírus, fungos e por reações alérgicas) no espaço alveolar, onde ocorre a troca gasosa. Esse local deve estar sempre muito limpo, livre de substâncias que possam impedir o contato do ar com o sangue.\r\nDiferentes do vírus da gripe, que é altamente infectante, os agentes infecciosos da pneumonia não costumam ser transmitidos facilmente.'),
(5, 'Diabetes mellitus', '', 'Diabetes Mellitus é uma doença caracterizada pela elevação da glicose no sangue (hiperglicemia). Pode ocorrer devido a defeitos na secreção ou na ação do hormônio insulina, que é produzido no pâncreas, pelas chamadas células beta . A função principal da insulina é promover a entrada de glicose para as células do organismo de forma que ela possa ser aproveitada para as diversas atividades celulares. A falta da insulina ou um defeito na sua ação resulta portanto em acúmulo de glicose no sangue, o que chamamos de hiperglicemia.'),
(6, 'Doenças hipertensivas', '', 'Emergência hipertensiva é a hipertensão grave com sinais de comprometimento de órgãos-alvo (cérebro, sistema cardiovascular e rins). Efetua-se o diagnóstico pela avaliação de PA, ECG, análise da urina, bem como de ureia e creatinina séricas. O tratamento consiste na redução imediata da PA com drogas IV (p. ex., clevidipino, fenoldopam, nitroglicerina, nitroprussiato, nicardipino, labelol, esmolol e hidralazina).'),
(7, 'Bronquite e asma', '', 'A asma é uma doença inflamatória crônica das vias aéreas que acomete mais de 300 milhões de pessoas no mundo. Ela leva a um estreitamento das vias aéreas, o que prejudica o fluxo de ar que entra e sai dos pulmões e provoca falta de ar e chiado no peito.\r\n\r\nA bronquite também é uma inflamação dos brônquios que pode ser aguda. Geralmente, é provocada por um vírus e acompanhada de um quadro gripal com duração de, em média, duas semanas. A bronquite também pode ser crônica (uma inflamação geralmente causada pelo fumo).'),
(8, 'Insuficiência cardíaca', '', 'Insuficiência cardíaca é um distúrbio em que o coração não consegue suprir as necessidades do corpo, causando redução do fluxo sanguíneo, refluxo (congestão) de sangue nas veias e nos pulmões e/ou outras alterações que podem debilitar ou enrijecer ainda mais o coração.'),
(9, 'Câncer de pulmão', '', 'Câncer de pulmão é um dos tumores malignos mais comuns. A doença pode ser de dois tipos diferentes: o de pequenas células e o de não pequenas células (o tipo mais frequente).'),
(10, 'Cirrose e doenças crônicas fígado', '', 'Cirrose é uma doença crônica do fígado que se caracteriza por fibrose e formação de nódulos que bloqueiam a circulação sanguínea. Pode ser causada por infecções ou inflamação crônica dessa glândula. A cirrose faz com que o fígado produza tecido de cicatrização no lugar das células saudáveis que morrem. Com isso, ele deixa de desempenhar suas funções normais como produzir bile (um agente emulsificador de gorduras), auxiliar na manutenção dos níveis normais de açúcar no sangue, produzir proteínas, metabolizar o colesterol, o álcool e alguns medicamentos, entre outras.'),
(11, 'Câncer de estômago', '', 'Não há sintomas específicos do câncer de estômago. Porém, alguns sinais, como perda de peso e de apetite, fadiga, sensação de estômago cheio, vômitos, náuseas e desconforto abdominal persistente podem indicar tanto uma doença benigna (úlcera, gastrite, etc.) como um tumor de estômago.');

-- --------------------------------------------------------

--
-- Estrutura da tabela `fale_conosco`
--

CREATE TABLE IF NOT EXISTS `fale_conosco` (
  `id` int(220) NOT NULL AUTO_INCREMENT,
  `nome` varchar(220) NOT NULL,
  `email` varchar(220) NOT NULL,
  `telefone` varchar(14) NOT NULL,
  `mensagem` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `fale_conosco`
--

INSERT INTO `fale_conosco` (`id`, `nome`, `email`, `telefone`, `mensagem`) VALUES
(1, 'tiago', 'tiago@gmail.com', '48996511649', 'nananana'),
(2, 'tiago', 'tiago@gmail.com', '34425437', 'arruma ai');

-- --------------------------------------------------------

--
-- Estrutura da tabela `medico`
--

CREATE TABLE IF NOT EXISTS `medico` (
  `id_medico` int(11) NOT NULL AUTO_INCREMENT,
  `id_doenca` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  PRIMARY KEY (`id_medico`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(200) NOT NULL,
  `senha` varchar(32) NOT NULL,
  `email` varchar(200) NOT NULL,
  `privilegio` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `usuario`, `senha`, `email`, `privilegio`) VALUES
(14, 'robson', '123', 'robshow@hotmail.com', 0),
(15, 'carlos', 'car12', 'carlos@gmail.com', 0),
(13, 'tiago', 'paiano001', 'tiago@gmail.com', 1),
(16, 'carlinhos', 'caca123', 'carlinhos@gmail.com', 0),
(17, 'aa', '123', 'aa@gmail.com', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
