<!DOCTYPE html>
<html lang="pt-br">

<!---------------ICONE DO SITE----------------->
<link rel="shortcut icon" href="img/logo.png" />
<!--------------------------------------------->

<!-- Início dos metadados -->
<head>
<!--------------------Titulo do site------------------->
<title>Web Clinic</title>
<!----------------------------------------------------->

 <link rel="stylesheet" href="css/bootstrap.min.css">

 <!-----------Para aceitar acentuacoes-------------->
 <meta charset="utf-8">
 <!------------------------------------------------->

 <!-----------Onde chama o CSS DA PAGINA------------>
    <link rel="stylesheet" href="login.css">
  <!------------------------------------------------>
</head>
<!-- Fim dos metadados -->

<!-- Corpo da página -->
<!----------------------->
<head>
  
<!---------------------------CHAMANDO A FONTE DO NAV--------------------------------->
<link rel="stylesheet" href="estilo2.css">
<link href="https://fonts.googleapis.com/css?family=Exo&display=swap" rel="stylesheet">
<!------------------------------------------------------------------------------------>

<!-------------------------------NAV------------------------------------------------------------->
<!------------------------------COR DO NAV------------------------->
<nav class="navbar navbar-expand-lg navbar navbar-primary bg-body">
<!----------------------------------------------------------------->

  <div class="container">
    <!-------------------------------LOGO DO SITE NO NAV---------------------------------->
      <a class="navbar-brand" href=""><img src="img/logo.png" width="40" height="40"></a>
      <!---------------------------------------------------------------------------------->
    <!----------------NOME DO SITE no NAV----------------->
    <a class="navbar-brand" href=""><B>Web Clinic</B></a>
    <!---------------------------------------------------->

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu_resp">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="menu_resp">
     <ul class="navbar-nav nav ml-auto">

       <li class="nav-item"><a href="indexLocal.php" class="nav-link">Home</a></li>

          <li class="nav-item"><a href="login.php" class="nav-link">Login</a></li>

         <li class="nav-item"><a href="sobre.php" class="nav-link">Sobre</a></li>

<li class="nav-item dropdown">
       <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Solicitar a consulta</a>
       <div class="dropdown-menu bg-muted" id="submenu">
          <a href="calendario.php" class="dropdown-item" >Agende</a>
          <a href="#" class="dropdown-item" >SUB TEMA 1.1</a>
          <a href="calendario.php" class="dropdown-item" >Agende</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="sobre.php">Sobre</a>
       </div>
      </li>

        <li class="nav-item"><a href="contato.php" class="nav-link">Fale conosco</a></li>

    <li class="nav-item"><a href="admin.php" class="nav-link">Adm</a></li>
    
		<a href=#><img src="img/facebook.png"><i class="fa fa-facebook"></i></a>
        <a href=#><img src="img/instagram.png"><i class="fa fa-instagram"></i></a>
      </ul>
      </div>
  </div>
</nav>
<!-----------------------------------------------FIM Do NAV---------------------------------------------------------> 

<!------------------------------------------------INICIO DO LOGIN--------------------------------------------------->

<head>
 <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
  <link rel="stylesheet" type="text/css" href="login.css" />
</head>
<body>
  <br><br><br><br>
  <div class="container" >
    <a class="links" id="paralogin"></a>
    <a class="links" id="paracadastro"></a>
     <div class="content">  

      <!--FORMULÁRIO DE CADASTRO-->

      <div id="cadastro">
        <form method="post" action="cadastro.php"> 
          <h1>Cadastro</h1> 
           
          <p> 
            <label for="nome_cad">Seu nome</label>
            <input id="nome_cad" name="nome" required="required" type="text" placeholder="nome" />
          </p>
           
          <p> 
            <label for="email_cad">Seu e-mail</label>
            <input id="email_cad" name="email" required="required" type="email" placeholder="contato@htmlecsspro.com"/> 
          </p>
           
          <p> 
            <label for="senha_cad">Sua senha</label>
            <input id="senha_cad" name="senha" required="required" type="password" placeholder="ex. 1234"/>
          </p>
           
          <p> 
            <input type="submit" value="Cadastrar"/> 
          </p>
           
          <p class="link">  
            Já tem conta?
            <a href="#paralogin"> Ir para Login </a>
          </p>
        </form>
      </div>

      <!--FORMULÁRIO DE LOGIN-->

      <div id="login">
        <form method="post" action="controle.php"> 
          <h1>Login</h1> 
          <p> 
            <label for="nome_login">Seu e-mail</label>
            <input id="nome_login" name="email" required="required" type="text" placeholder="ex. contato@htmlecsspro.com"/>
          </p>
           
          <p> 
            <label for="email_login">Sua senha</label>
            <input id="email_login" name="senha" required="required" type="password" placeholder="ex. senha" /> 
          </p>
           
          <p> 
            <input type="checkbox" name="manterlogado" id="manterlogado" value="" /> 
            <label for="manterlogado">Manter-me logado</label>
          </p>
           
          <p> 
            <input type="submit" value="Logar" /> 
          </p>
           
          <p class="link">
            Ainda não tem conta?
            <a href="#paracadastro">Cadastre-se</a>
          </p>
        </form>
      </div>   
 
    </div>
  </div>  
  </body>
  </body>
<!-----------------------------------------------------FIM DO LOGIN--------------------------------------------------------->
<!--Fim do corpo da página -->


	<!-- Meta tags Obrigatórias -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
  
    <!-- JavaScript (Opcional) -->
    <!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>