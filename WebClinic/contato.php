<!doctype html>
<html lang="pt-br">

<!------------------LOGO NA BARRA DE GUIA------------------>
<link rel="shortcut icon" href="img/logo.png" />
<!--------------------------------------------------------->

<head>
  <!-----------Para aceitar acentuacoes-------------->
   <meta charset="UTF-8" />
  <!------------------------------------------------>

  <link rel="stylesheet" href="css/bootstrap.min.css">
<!------------------------------------------------------------>

<!----------------------Fonte do NAV---------------------------------------------------->
<link rel="stylesheet" href="estilo2.css">
<link href="https://fonts.googleapis.com/css?family=Exo&display=swap" rel="stylesheet">
<!-------------------------------------------------------------------------------------->


<!---------------------------------------NAV----------------------------------------------------------------->

<!------------------------------COR do NAV------------------------->
<nav class="navbar navbar-expand-lg navbar navbar-primary bg-body">
  <!--------------------------------------------------------------->

  <div class="container">
    
    <!----------------------------------LOGO NO NAV--------------------------------------->
      <a class="navbar-brand" href=""><img src="img/logo.png" width="40" height="40"></a>
    <!------------------------------------------------------------------------------------>

    <!-----------------Nome "WEB CLINIC" no NAV------------->
    <a class="navbar-brand" href=""><B>Web Clinic</B></a>
    <!------------------------------------------------------>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu_resp" >
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="menu_resp">
     <ul class="navbar-nav nav ml-auto">

       <li class="nav-item"><a href="indexLocal.php" class="nav-link">Home</a></li>

          <li class="nav-item"><a href="login.php" class="nav-link">Login</a></li>

         <li class="nav-item"><a href="sobre.php" class="nav-link">Sobre</a></li>
       
<li class="nav-item dropdown">
       <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Solicitar a consulta</a>
       <div class="dropdown-menu bg-muted" id="submenu">
          <a href="calendario.php" class="dropdown-item" >Agende</a>
          <a href="#" class="dropdown-item" >SUB TEMA 1.1</a>
          <a href="calendario.php" class="dropdown-item" >Agende</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="sobre.php">Sobre</a>
       </div>
      </li>

        <li class="nav-item"><a href="contato.php" class="nav-link">Fale conosco</a></li>

    <li class="nav-item"><a href="admin.php" class="nav-link">Adm</a></li>
    
		<a href=#><img src="img/facebook.png"><i class="fa fa-facebook"></i></a>
        <a href=#><img src="img/instagram.png"><i class="fa fa-instagram"></i></a>

      </ul>
      </div>
  </div>
</nav>
<!---------------------------------------------------------FIM NAV--------------------------------------------->

<!-- Bootstrap -->
<style type="text/css">
  input,textarea, button{<br/> margin: 10px 0 0px 0;<br/>}<br/></style>
&nbsp;

<!-----------------------------------------COR DE FUNDO DO CONTAINER DO FALE CONOSCO------------------------------------>
<div class="container" style="margin-top: 5px; background-color: #7eda52; translate (-50%, -50%); border-radius: 20px">
<!---------------------------------------------------------------------------------------------------------------------->
<br><br>
<h2>Fale Conosco</h2>
<div class="row">
<div class="col-sm-6">
<br><br>

<!-- Aqui abro a tag form e defino a action e o metodo a ser enviado Action = P�gina destino Method = Post (envia o c�digo de maneira "n�o vis�vel na url" -->

<form action="fale_conosco.php" method="post"><!-- input do formulario -->
<h6>Nome Completo</h6>
<div class="row">
<div class="col-sm-12"><input id="nome" class="form-control" name="nome" required="" type="text" placeholder="Nome"/></div>
</div>

<br>
<h6>Telefone de uso</h6>
<div class="row">
<div class="col-sm-12"><input id="telefone" class="form-control" name="telefone" required="" type="text" placeholder="Telefone"/></div>
</div>

<br>
<h6>E-mail principal</h6>
<div class="row">
<div class="col-sm-12"><input id="email" class="form-control" name="email" required="" type="email" placeholder="email"/></div>
</div>

<br>
<h6>Mensagem</h6>
<div class="row">
<div class="col-sm-12"><textarea id="mensagem" class="form-control" name="mensagem" required="" placeholder="Duvida, sugestoes, criticas negativas ou negativas poste aqui que resolveremos o mais rapido possivel"></textarea></div>
</div>

<br>
<div class="row">
<div class="col-sm-12"><button class="btn btn-success" type="submit">Enviar</button></div>
</div>

</form>

</div>
<div class="col-sm-6">
<br></br><br></br><br></br>
<h5>Telefone</h5>
48 99999-9999
<hr />
<h5>E-mail</h5>
webclinic@outlook.com.br
<hr/>
</div>
</div>
</div>
<!-- Bootstrap -->


<!----------------------------------------------------------FIM FALE CONOSCO---------------------------------------------------------------->

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

<!-- Meta tags Obrigat�rias -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
   <title>Web Clinic</title>
  </head>
  <body>
  
    <!-- JavaScript (Opcional) -->
    <!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>



	<!-- Meta tags Obrigat�rias -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
   <title>Web Clinic</title>
  </head>
  <body>
  
    <!-- JavaScript (Opcional) -->
    <!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>