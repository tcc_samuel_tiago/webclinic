﻿<!---------------Mostra o botao "SAIR" se o usuario estiver logado--------------->
<?php
@session_start();

$logado = false;

if(isset($_SESSION['id_usuario'])){
  $logado = true;
}
?>
<!-------------------------------------------------------------------------------->

<!doctype html>
<html lang="pt-br">

<!------------------LOGO NA BARRA DE GUIA------------------>
<link rel="shortcut icon" href="img/logo.png" />
<!--------------------------------------------------------->

<!---------------------CSS sobre---------------------->
<head>
 <link rel="stylesheet" href="css/bootstrap.min.css">
 <meta charset="utf-8">
    <title>Web Clinic</title>
    <link rel="stylesheet" href="estilo2.css">
</head>
<!----------------------------------------------------->

  <head>
  <!-----------Para aceitar acentuacoes-------------->
   <meta charset="UTF-8" />
  <!------------------------------------------------>

  <link rel="stylesheet" href="css/bootstrap.min.css">
<!------------------------------------------------------------>

<!----------------------Fonte do NAV---------------------------------------------------->
<link rel="stylesheet" href="estilo2.css">
<link href="https://fonts.googleapis.com/css?family=Exo&display=swap" rel="stylesheet">
<!-------------------------------------------------------------------------------------->


<!---------------------------------------NAV----------------------------------------------------------------->

<!------------------------------COR do NAV------------------------->
<nav class="navbar navbar-expand-lg navbar navbar-primary bg-body">
  <!--------------------------------------------------------------->

  <div class="container">
    
    <!----------------------------------LOGO NO NAV--------------------------------------->
      <a class="navbar-brand" href=""><img src="img/logo.png" width="40" height="40"></a>
    <!------------------------------------------------------------------------------------>

    <!-----------------Nome "WEB CLINIC" no NAV------------->
    <a class="navbar-brand" href=""><B>Web Clinic</B></a>
    <!------------------------------------------------------>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu_resp" >
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="menu_resp">
     <ul class="navbar-nav nav ml-auto">

       <li class="nav-item"><a href="indexLocal.php" class="nav-link">Home</a></li>

          <li class="nav-item"><a href="login.php" class="nav-link">Login</a></li>

         <li class="nav-item"><a href="sobre.php" class="nav-link">Sobre</a></li>
       
<li class="nav-item dropdown">
       <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Solicitar a consulta</a>
       <div class="dropdown-menu bg-muted" id="submenu">
          <a href="calendario.php" class="dropdown-item" >Agende</a>
          <a href="#" class="dropdown-item" >SUB TEMA 1.1</a>
          <a href="calendario.php" class="dropdown-item" >Agende</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="sobre.php">Sobre</a>
       </div>
      </li>

        <li class="nav-item"><a href="contato.php" class="nav-link">Fale conosco</a></li>

    <li class="nav-item"><a href="admin.php" class="nav-link">Adm</a></li>
    
		<a href=#><img src="img/facebook.png"><i class="fa fa-facebook"></i></a>
        <a href=#><img src="img/instagram.png"><i class="fa fa-instagram"></i></a>

      </ul>
      </div>
  </div>
</nav>
<!---------------------------------------------------------FIM NAV--------------------------------------------->
<!----------------------Imagem de fundo-------------------->

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="img/ww.jpg" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
      <img src="img/8.jpg" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
      <img src="img/7.jpg" class="d-block w-100" alt="...">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>




<!------------------------FIM------------------------------>
	<!-- Meta tags Obrigatórias -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
   <title>Web Clinic</title>
  </head>
  <body>
  
    <!-- JavaScript (Opcional) -->
    <!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>