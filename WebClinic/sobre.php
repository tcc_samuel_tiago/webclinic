<!doctype html>
<html lang="pt-br">

<!------------------LOGO NA BARRA DE GUIA------------------>
<link rel="shortcut icon" href="img/logo.png" />
<!--------------------------------------------------------->

<!---------------------CSS sobre---------------------->
<head>
 <link rel="stylesheet" href="css/bootstrap.min.css">
 <meta charset="utf-8">
    <title>Web Clinic</title>
    <link rel="stylesheet" href="sobre.css">
</head>
<!----------------------------------------------------->

  <head>
  <!-----------Para aceitar acentuacoes-------------->
   <meta charset="UTF-8" />
  <!------------------------------------------------>

  <link rel="stylesheet" href="css/bootstrap.min.css">
<!------------------------------------------------------------>

<!----------------------Fonte do NAV---------------------------------------------------->
<link rel="stylesheet" href="estilo2.css">
<link href="https://fonts.googleapis.com/css?family=Exo&display=swap" rel="stylesheet">
<!-------------------------------------------------------------------------------------->


<!---------------------------------------NAV----------------------------------------------------------------->
<!------------------------------Cor do NAV------------------------->
<nav class="navbar navbar-expand-lg navbar navbar-primary bg-body">
  <!--------------------------------------------------------------->

  <div class="container">
    
    <!----------------------------------LOGO NO NAV--------------------------------------->
      <a class="navbar-brand" href=""><img src="img/logo.png" width="40" height="40"></a>
    <!------------------------------------------------------------------------------------>

    <!-----------------Nome "WEB CLINIC" no NAV------------->
    <a class="navbar-brand" href=""><B>Web Clinic</B></a>
    <!------------------------------------------------------>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu_resp" >
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="menu_resp">
     <ul class="navbar-nav nav ml-auto">

       <li class="nav-item"><a href="indexLocal.php" class="nav-link">Home</a></li>

          <li class="nav-item"><a href="login.php" class="nav-link">Login</a></li>

         <li class="nav-item"><a href="sobre.php" class="nav-link">Sobre</a></li>
       
<li class="nav-item dropdown">
       <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Solicitar a consulta</a>
       <div class="dropdown-menu bg-muted" id="submenu">
          <a href="calendario.php" class="dropdown-item" >Agende</a>
          <a href="#" class="dropdown-item" >SUB TEMA 1.1</a>
          <a href="calendario.php" class="dropdown-item" >Agende</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="sobre.php">Sobre</a>
       </div>
      </li>

        <li class="nav-item"><a href="contato.php" class="nav-link">Fale conosco</a></li>

    <li class="nav-item"><a href="admin.php" class="nav-link">Adm</a></li>
    
		<a href=#><img src="img/facebook.png"><i class="fa fa-facebook"></i></a>
        <a href=#><img src="img/instagram.png"><i class="fa fa-instagram"></i></a>

      </ul>
      </div>
  </div>
</nav>
<!---------------------------------------------------------FIM NAV--------------------------------------------->

<!----------------------------------------------------INICIO DO SOBRE-------------------------------------------------------------------------->
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
  <!-----------------------------------------------------CARDS--------------------------------------------------------------------------------->
    <div class="card-deck">
      <div class="card">
        <img src="img/01.png" class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title1">AGENDAMENTOS</h5>
          <p class="card-text">Este card ira direcionar voce ate um calendario de agendamento para voce ter seu atendimento rapido e facil.</p>
		  <a href="calendario.php" class="btn btn-success">CLIQUE AQUI</a>
          <p class="card-text"><small class="text-muted">2019</small></p>
        </div>
      </div>
	  
      <div class="card">
        <img src="img/02.png" class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title1">MEDICOS</h5>
          <p class="card-text">Este card ira direcionar voce para uma aba onde voce podera encontrar os melhores medicos da regiao que possa atender a sua necessidade.</p>
		  <a href="indexLocal.php" class="btn btn-success">CLIQUE AQUI</a>
          <p class="card-text"><small class="text-muted">2019</small></p>
        </div>
      </div>
	  
      <div class="card">
        <img src="img/04.png" class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title1">CONTATO</h5>
          <p class="card-text">Este card ira direcionar voce para ima pagina onde voce podera interagir com a gente e enviar suas opinioes.</p>
		  <a href="contato.php" class="btn btn-success">CLIQUE AQUI</a>
          <p class="card-text"><small class="text-muted">2019</small></p>
        </div>
      </div>
    </div>
    <!-------------------------------------------------------------Final dos CARDS-------------------------------------------------------------->
    <br> 
    <br>
    <br>
    </div>
  </head>
<!------------------------------------------------------------FIM DO SOBRE-------------------------------------------------------------------->

	<!-- Meta tags Obrigatórias -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
   <title>Web Clinic</title>
  </head>
  <body>
  
    <!-- JavaScript (Opcional) -->
    <!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>